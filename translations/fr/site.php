<?php

return array(
	//Home
	"Walk-in\nclinic"=>"Clinique\nsans rendez-vous",
	"Appointment\nwith your doctor"=>"Rendez-vous\navec votre médecin",
	"Find a\nfamily doctor"=>"Trouver un\nmédecin de famille",
	"Learn more"=>"En savoir plus",
	//Services
	"Regular Appointment"=>"Rendez-vous\nrégulier",
	"Emergency Appointment"=>"Rendez-vous\nurgent",
	"Walk-in Clinic"=>"Sans\nrendez-vous",
	"Family Doctor Care"=>"Prise en\ncharge",
	"Our team"=>"Notre équipe",


	"Cover letter"=>"Lettre de présentation",
	"Send"=>"Soumettre",

	"In person: "=>"En personne : ",
	"Name"=>"Nom",
	"Phone number"=>"Numéro de Téléphone",
	"Email"=>"Courriel",
	"Phone: "=>"Téléphone : ",
	"Phone"=>"Téléphone",
	"Contact information"=>"Coordonnées",
	"Attach your curriculum vitae"=>"Joignez votre curriculum vitae",
	"Attach your cover letter"=>"Joignez votre lettre de présentation",

	"All rights reserved"=>"Tous droits réservés",
	"Fax: "=>"Télécopieur : ",
	"Fax"=>"Télécopieur : ",
	"Toll free: "=>"Sans frais : ",
	"Monday to Friday: "=>"Lundi au vendredi : ",
	"Saturday and Sunday: "=>"Samedi et dimanche : ",
	"Your message has been sent successfully. We will contact you if your profile matches one of our positions."=>"Votre message a été envoyé avec succès. Nous vous contacterons si votre profil correspond à l’un de nos postes.",
	"Join our team"=>"Joignez notre équipe",
	"Contact us"=>"Nous joindre", 
	"Visit us"=>"Visitez-nous",
	"All rights reserved"=>"Tous droits réservés",
	"Signed Zel"=>"Signé Zel",
	"available jobs"=>"Postes disponibles",
	"Job description"=>"Description du poste",
	"Qualifications & knowledge"=>"Qualifications & connaissances",
	"Benefits"=>"Avantages",
	"Apply now"=>"Postulez maintenant",
	"All"=>"Tous",
	"Our projects"=>"Nos projets",
	"Contact us"=>"Écrivez-nous",
	"Your name"=>"Votre nom",
	"Our services"=>"Nos services",
	"Adress"=>"Adresse",
	"This field is required."=>"Ce champ est obligatoire."


);